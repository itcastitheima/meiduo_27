from celery import Celery

#celery服务器在运行的过程中要加载该工程的配置文件
import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mall.settings'

#创建celery对象
app = Celery('celery_tasks')

#加载配置文件
app.config_from_object('celery_tasks.config')

#自动检测任务
app.autodiscover_tasks(['celery_tasks.sms'])