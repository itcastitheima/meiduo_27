
from libs.yuntongxun.sms import CCP
from celery_tasks.main import app

@app.task
def send_sms_code(mobile,smscode):
    ccp = CCP()
    ccp.send_template_sms(mobile, [smscode, 5], 1)