from rest_framework import serializers
from django_redis import get_redis_connection
from redis import RedisError
import logging
logger = logging.getLogger('meiduo')

class SMSCodeSerializer(serializers.Serializer):

    text = serializers.CharField(max_length=4,min_length=4)
    image_code_id = serializers.UUIDField()

    def validate(self, attrs):

        #获取用户提交的验证码
        text = attrs['text']
        #获取redis的验证码,并判断是否存在
        redis_conn = get_redis_connection('code')
        redis_text = redis_conn.get('img_%s'%attrs['image_code_id'])

        #记得删除图片
        try:
            redis_conn.delete('img_%s'%attrs['image_code_id'])
        except RedisError as e:
            logger.log(e)

        if  not redis_text:
            raise serializers.ValidationError('验证码过期')
        #进行比较,比较的时候注意,redis返回的数据的bytes类型
        if redis_text.decode().lower() != text.lower():
            raise  serializers.ValidationError('验证码错误')
        #返回结果
        return attrs