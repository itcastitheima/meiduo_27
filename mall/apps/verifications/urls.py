from django.conf.urls import url
from . import views

urlpatterns = [
    #verify/imagecode/image_code_id/
    url(r'^imagecodes/(?P<image_code_id>.+)/$',views.ImageCodeView.as_view()),
    #/verify/smscodes/(?P<mobile>1[345789]\d{9})/
    url(r'^smscodes/(?P<mobile>1[345789]\d{9})/$',views.SMSCodeView.as_view()),
]