from django.shortcuts import render
from rest_framework.views import APIView
from libs.captcha.captcha import captcha
from django_redis import get_redis_connection
from django.http import HttpResponse
from rest_framework.generics import GenericAPIView
from . import serializers
import random
from libs.yuntongxun.sms import CCP
from rest_framework.response import Response
from rest_framework import status
from celery_tasks.sms.tasks import send_sms_code

# Create your views here.

class SMSCodeView(GenericAPIView):
    """
    校验验证码,并且发送短信
    GET /verify/smscodes/(?P<mobile>1[345789]\d{9})/?text=xxxx&image_code_id=xxxx
    """
    serializer_class = serializers.SMSCodeSerializer
    def get(self,request,mobile):
        #校验验证码
        serializer = serializers.SMSCodeSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        #生成短信验证码
        smscode = '%06d'%random.randint(0,999999)
        #redis记录
        redis_conn = get_redis_connection('code')
        flag = redis_conn.get('sms_flag_%s'%mobile)
        if flag:
            return Response({'message':'操作太频繁'},status=status.HTTP_429_TOO_MANY_REQUESTS)
        pl = redis_conn.pipeline()
        pl.multi()
        pl.setex('sms_%s'%mobile,5*30,smscode)
        pl.setex('sms_flag_%s'%mobile,60,1)
        pl.execute()
        #发送
        # ccp = CCP()
        # ccp.send_template_sms(mobile,[smscode,5],1)
        send_sms_code.delay(mobile,smscode)
        #返回响应
        return Response({'message','ok'})

class ImageCodeView(APIView):
    """
    生成验证码图片
    GET:  /verify/imagecode/image_code_id/
    """
    def get(self,request,image_code_id):
        #通过captcha 生成图片和图片的验证码
        text,image = captcha.generate_captcha()

        #将验证码信息通过redis记录下来,以便验证
        redis_conn = get_redis_connection('code')
        redis_conn.setex('img_%s'%image_code_id,60,text)

        #返回图片
        return HttpResponse(image,content_type='image/jpeg')
