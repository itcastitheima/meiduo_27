from django.conf.urls import url
from . import views
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    #/users/username/(?P<username>\w{5,20})/count/
    url(r'^username/(?P<username>\w{5,20})/count/$',views.RegiseterUsernameCountView.as_view()),
    #/users/mobile/(?P<mobile>1[345789]\d{9})/count/
    url(r'^mobile/(?P<mobile>1[345789]\d{9})/count/$',views.RegisterMobileCountView.as_view()),
    #/userr/
    url(r'^$',views.RegisterUserView.as_view()),
    #/users/r'authorizations/
    url(r'^authorizations/$',obtain_jwt_token),

]