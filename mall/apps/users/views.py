from django.shortcuts import render
from rest_framework.views import APIView
from .models import User
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView,GenericAPIView
from . import serializers
# Create your views here.


class RegisterUserView(CreateAPIView):
    """
    注册视图
    POST /users/
    """
    serializer_class = serializers.RegisterUserSerializer

class RegisterMobileCountView(APIView):
    """
    获取手机号的个数
    GET /users/mobile/(?P<mobile>1[345789]\d{9})/count/
    """
    def get(self,request,mobile):

        #查询个数
        count = User.objects.filter(mobile=mobile).count()
        #返回响应
        context = {
            'count':count,
            'mobile':mobile
        }
        return Response(context)

class RegiseterUsernameCountView(APIView):
    """
    返回用户名的个数
    GET /users/username/(?P<username>\w{5,20})/count/
    """
    def get(self,request,username):

        #查询用户名个数
        count = User.objects.filter(username=username).count()
        #返回数据(个数,用户名)
        context = {
            'count':count,
            'username':username
        }

        return Response(context)