from rest_framework import serializers
from .models import User
import re
from django_redis import get_redis_connection


class RegisterUserSerializer(serializers.ModelSerializer):

    allow = serializers.CharField(label='是否同意协议',required=True,allow_null=False,allow_blank=False,write_only=True)
    password2 = serializers.CharField(label='确认密码',required=True,allow_null=False,allow_blank=False,write_only=True)
    sms_code = serializers.CharField(label='验证码',max_length=6,min_length=6,required=True,allow_null=False,allow_blank=False,write_only=True)
    token = serializers.CharField(label='登录状态token', read_only=True)  # 增加token字段

    #进行校验,校验密码,校验手机号,校验是否同意协议
    def validate_mobile(self,value):

        if not re.match('1[345789]\d{9}',value):
            raise serializers.ValidationError('手机号不正确')
        return value

    def validate_allow(self,value):
        if value != 'true':
            raise serializers.ValidationError('未同意协议')
        return value

    def validate(self, attrs):

        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError('密码不一致')

        #验证码校验
        redis_conn = get_redis_connection('code')
        redis_code = redis_conn.get('sms_%s'%attrs['mobile'])
        if redis_code is None:
            raise serializers.ValidationError('验证码已过期')
        #注意redis的value都是bytes类型
        if redis_code.decode() != attrs['sms_code']:
            raise serializers.ValidationError('验证码错误')
        return attrs

    def create(self, validated_data):

        #删除多于数据
        del validated_data['sms_code']
        del validated_data['allow']
        del validated_data['password2']
        #数据入库,修改密码入库方式
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        #返回 jwt
        # 补充生成记录登录状态的token
        from rest_framework_jwt.settings import api_settings
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'password2', 'sms_code', 'mobile', 'allow','token')
        extra_kwargs = {
            'id': {'read_only': True},
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }
